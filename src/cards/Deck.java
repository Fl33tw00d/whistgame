///////////////////////////////////////////////////////////////////////////////
//                   
// Main Class File:  Deck.java
// File:             Deck.java
// Date:             23/01/2019
//
// Author:           Christopher Fleetwood
// Student Number:   100228021
//
//
// Description: This class is designed to model a deck of cards, which is a 
//              collection of card objects. It also provides useful methods for
//              manipulation of the deck.
//              
// Credits:          none
//
//////////////////////////// 80 columns wide //////////////////////////////////
package cards;

import cards.Card.Suit;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 *
 * @author fleet
 */
public class Deck extends ArrayList<Card> implements Iterable<Card>, Serializable {

    //Overriding the add method to ensure that the ArrayList is of fixed size
    //as stated in the description
    @Override
    public boolean add(Card c) {
        if (this.size() < 52) {
            return super.add(c);
        }
        System.out.println("You are trying to exceed the 52 card limit.");
        return false;
    }

    //INSTANCE VARIABLES
    private static final long serialVersionUID = 49;
    ArrayList<Card> cards;
    ArrayList<Card> deckOfSpades;

    //Deck constructor that takes no arguements, and returns a filled 
    //deck and shuffles it
    public Deck() {
        cards = new ArrayList<>();
        for (Card.Rank r : Card.Rank.values()) {
            for (Card.Suit s : Card.Suit.values()) {
                cards.add(new Card(r, s));
            }
        }
        Collections.shuffle(cards);
    }

    //Deck constructor that takes an array list as an arguement and wraps it 
    //in a Deck object
    public Deck(ArrayList<Card> AL) {
        cards = AL;
    }

    //returns the remaining number of cards in the deck
    public int size() {
        return cards.size();
    }

    //Clears the previous card list and fills it again with 
    //all possible cards and shuffles
    public void newDeck(Deck d) {
        //Clears the current deck
        cards.clear();
        //Adds the new set of cards to the deck
        for (Card.Rank r : Card.Rank.values()) {
            for (Card.Suit s : Card.Suit.values()) {
                cards.add(new Card(r, s));
            }
        }
        Collections.shuffle(cards);
    }

    //Prints out all cards in the deck
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Card c : cards) {
            result.append(c + "\n");
        }

        return result.toString();
    }

    private class DeckIterator<Card> implements Iterator<Card> {

        int pos = cards.size();

        @Override
        public boolean hasNext() {
            if (pos > 0) {
                return true;
            }
            return false;
        }

        @Override
        public Card next() {
            return (Card) cards.get(--pos);
        }

        @Override
        public void remove() {
            cards.remove(pos);
        }
    }

    @Override
    public Iterator<Card> iterator() {
        return new DeckIterator();
    }

    private class SpadeIterator<Card> implements Iterator<Card> {

        int pos = deckOfSpades.size();

        @Override
        public boolean hasNext() {
            if (pos > 0) {
                return true;
            }
            return false;
        }

        @Override
        public Card next() {
            return (Card) deckOfSpades.get(--pos);
        }

    }

    //Returns an instance of the SpadeIterator
    public Iterator<Card> Spadeiterator() {
        return new SpadeIterator();
    }

    //Deals a card using the custom iterator in order to take the card from
    //the top of the deck
    public Card deal() {
        Card cardToDeal = null;
        Iterator<Card> it = iterator();

        if (it.hasNext()) {
            cardToDeal = it.next();
            it.remove();
        } else {
            System.out.println("No Cards Remaining");
        }
        return cardToDeal;
    }

    //Method to go through the deck and only return the SPADES
    //This allows for the spadeIterator to only access SPADES
    public void createDeckOfSpades() {
        deckOfSpades = new ArrayList<>();
        for (Card c : this.cards) {
            if (c.getSuit() == Card.Suit.SPADES) {
                deckOfSpades.add(c);
            }
        }
    }

    //A method that fills a deck of spades using the spade iterator 
    //and then saves this object to a file SPADES.ser
    public void saveSpades() {
        String filename = "spades.ser";

        Iterator<Card> spadeIt = Spadeiterator();
        while (spadeIt.hasNext()) {
            deckOfSpades.add(spadeIt.next());
        }

        try {
            FileOutputStream fos = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(fos);
            out.writeObject(deckOfSpades);
            out.close();
            fos.close();
            System.out.println("Serialized Data saved in: " + filename);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //TEST HARNESS
    public static void main(String[] args) {

        //Question 1
        System.out.println("QUESTION 1");
        Deck d = new Deck();
        Card overflow = new Card(Card.Rank.ACE, Card.Suit.SPADES);
        System.out.println("Deck creation: \n" + d);
        System.out.println("Trying to add card: " + overflow);
        d.add(overflow);
        //Question 2
        System.out.println("QUESTION 2");
        System.out.println("TEST DECK");
        System.out.println(d.toString());

        //Question 3
        System.out.println("QUESTION 3");
        System.out.println("The size of the deck is: " + d.size() + "\n");

        //Question 4
        System.out.println("QUESTION 4");
        Iterator<Card> it = d.iterator();
        System.out.println("Next card to be dealt: " + d.iterator().next() + "\n");
        //Question 5
        System.out.println("QUESTION 5");
        System.out.println("Dealing the next card: " + d.deal() + "\n");
        System.out.println("Deck After Dealing: \n" + d);
        //Question 6
        System.out.println("QUESTION 6");
        d.createDeckOfSpades();
        Iterator<Card> spadeIt = d.Spadeiterator();
        while (spadeIt.hasNext()) {
            System.out.println(spadeIt.next() + " ");
        }
        //Question 8
        System.out.println("QUESTION 8");
        System.out.println("Print serialVersionUID: " + serialVersionUID);
        Deck saveDeck = new Deck();
        saveDeck.createDeckOfSpades();
        saveDeck.saveSpades();

    }

}
