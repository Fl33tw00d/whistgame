///////////////////////////////////////////////////////////////////////////////                 
// Main Class File:  Hand.java
// File:             Hand.java
// Date:             23/01/2019
//
// Author:           Christopher Fleetwood
// Student Number:   100228021
//
// Description: This class is designed to model a hand of cards, which is a 
//              small collection of cards that each player will have. It has 
//              related useful methods.
//////////////////////////// 80 columns wide //////////////////////////////////
package cards;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

/**
 *
 * @author fleet
 */
public class Hand implements Iterable, Serializable {

    //Initializing the instance variables
    private static final long serialVersionUID = 300;
    private ArrayList<Card> hand;
    private ArrayList<Card> unsortedHand;
    private int[] handValues = new int[5];
    private int[] suitValues = new int[4];

    //Hand constructor with no arguments
    public Hand() {
        hand = new ArrayList<>();
        unsortedHand = new ArrayList<>();
    }

    //Hand constructor that takes an array of cards and adds them to the hand
    public Hand(Card[] arr) {
        hand = new ArrayList<>(Arrays.asList(arr));
        unsortedHand = new ArrayList<>(Arrays.asList(arr));
    }

    //Hand constructor that takes a different hand and adds all the cards
    //of that hand to the current hand
    public Hand(ArrayList<Card> H) {
        hand = new ArrayList<>();
        unsortedHand = new ArrayList<>();
        hand.addAll(H);
        unsortedHand.addAll(H);
    }

    //This method returns a boolean in the hand contains the passed suit
    public boolean hasSuit(Card.Suit suit) {
        int pos = 0;
        for (Card c : hand) {
            if (hand.get(pos).getSuit() == suit) {
                return true;
            }
            pos++;
        }
        return false;
    }

    //This method is used for counting the number of passed suit in the hand
    //and returning it as an integer
    public int countSuits(Card.Suit suit) {
        int numOfSuit = 0;
        int pos = 0;
        for (Card c : hand) {
            if (hand.get(pos).getSuit() == suit) {
                numOfSuit++;
            }
            pos++;
        }
        return numOfSuit;
    }

    //This method is used for counting the number of passed rank in the hand
    //and returning it as an integer
    public int countRank(Card.Rank rank) {
        int numOfRank = 0;
        int pos = 0;
        for (Card c : hand) {
            if (hand.get(pos).getRank() == rank) {
                numOfRank++;
            }
            pos++;
        }
        return numOfRank;
    }

    //This is the hand iterator that is used to iterate through the hand
    //in insertion order even if the hand has had a sort routine called upon it
    private class HandIterator<Card> implements Iterator<Card> {

        int pos = unsortedHand.size();

        @Override
        public boolean hasNext() {
            if (pos > 0) {
                return true;
            }
            return false;
        }

        @Override
        public Card next() {
            return (Card) unsortedHand.get(--pos);
        }

        @Override
        public void remove() {
            unsortedHand.remove(pos);
        }

    }

    //Instatiating our HandIterator when iterator is called
    @Override
    public Iterator<Card> iterator() {
        return new HandIterator();
    }

    //A method to add a passed card to a hand
    public void addCard(Card c) {
        hand.add(c);
        unsortedHand.add(c);
        calculateHandValue();
    }

    //A method to add a colleciton of cards to the hand 
    public void addCollection(ArrayList<Card> cards) {
        hand.addAll(cards);
        unsortedHand.addAll(cards);
        calculateHandValue();
    }

    //A method for adding a passed hand to the current hand
    public void addHand(Hand h) {
        this.hand.addAll(h.getHand());
        this.unsortedHand.addAll(h.getHand());
        calculateHandValue();
    }

    public ArrayList<Card> getHand() {
        return this.hand;
    }

    public boolean removeCard(Card c) {
        int i = 0;
        for (Card c1 : hand) {
            if (hand.get(i).equals(c)) {
                hand.remove(c);
                unsortedHand.remove(c);
                calculateHandValue();
                return true;
            }
            i++;
        }
        System.out.println("Card you are trying to remove is not present "
                + "in the hand");
        return false;
    }

    public boolean removeHand(Hand h) {
        if (this.hand.containsAll(h.hand)) {
            this.hand.removeAll(h.hand);
            return true;
        }
        return false;
    }

    //A method to remove a card at a specific passed index and calls the methods
    //to maintain the correct suit count
    public Card removeCardatIndex(int i) {
        //If a card exists at index i, then remove
        Card c = null;
        c = hand.get(i);
        hand.remove(i);
        unsortedHand.remove(i);
        calculateHandValue();

        return c;
    }

    //A method to calculate the sum of the rank values in the hand,
    //counting all possible versions with aces as lower or higher and saving 
    //the value to the hand value array
    private void calculateHandValue() {
        int temp;
        int aceCount = 0;
        handValues[0] = 0;
        Arrays.fill(suitValues, 0);
        //For every card in hand, if there is an ACE, add to the ace counter
        for (Card c : hand) {
            if (c.getValue() == 11) {
                aceCount++;
            }
            //Add the value of the card to the hand value array 
            temp = c.getValue();
            handValues[0] += temp;
            //Add the suit to the suit count array
            if (c.getSuit() == Card.Suit.CLUBS) {
                suitValues[0]++;
            } else if (c.getSuit() == Card.Suit.DIAMONDS) {
                suitValues[1]++;
            } else if (c.getSuit() == Card.Suit.HEARTS) {
                suitValues[2]++;
            } else {
                suitValues[3]++;
            }

        }

        //Method for filling the array of different value depending on if
        //an Ace is counted as low or high
        if (aceCount > 0) {
            for (int n = 1; n <= aceCount; n++) {
                handValues[n] = handValues[0] - (n * 10);
            }
        }
    }

    //Sorts the hand using the Card compareTo from Card
    public void sort() {
        Collections.sort(hand);
    }

    //Sorts the hand using the custom comparator CompareRank from 
    public void sortByRank() {
        Collections.sort(hand, new Card.CompareRank());
    }

    @Override
    public String toString() {
        StringBuilder handstring = new StringBuilder("     Hand  \n");
        for (Card c : hand) {
            handstring.append(c.toString()).append(System.lineSeparator());
        }
        return handstring.toString();
    }

    //Returns all cards in the hand that match the passed Suit
    public ArrayList<Card> getSuitedCards(Card.Suit s) {
        ArrayList<Card> suitedCards = new ArrayList<>();
        for (Card c : hand) {
            if (c.getSuit() == s) {
                suitedCards.add(c);
            }
        }
        return suitedCards;
    }

    //Returns all cards that match the rank passed
    public ArrayList<Card> getHighCards(Card.Rank r) {
        ArrayList<Card> highCards = new ArrayList<>();
        for (Card c : hand) {
            if (c.getRank() == r) {
                highCards.add(c);
            }
        }
        return highCards;
    }

    //Returns all cards in the hand that are  not the trump suit
    public ArrayList<Card> getNonTrumps(Card.Suit t) {
        ArrayList<Card> nonTrumps = new ArrayList<>();
        for (Card c : hand) {
            if (c.getSuit() != t) {
                nonTrumps.add(c);
            }
        }
        return nonTrumps;
    }

    public static void saveHand() {
        String filename = "hand.ser";
        //Creating test hand to serialize
        Hand h = new Hand();
        for (int n = 0; n < 5; n++) {
            Card c = new Card(Card.Rank.getRandomRank(), Card.Suit.getRandomSuit());
            h.addCard(c);
        }
        try {
            FileOutputStream fos = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(fos);
            out.writeObject(h);
            out.close();
            fos.close();
            System.out.println("Serialized Data saved in: " + filename);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {

        //Question 1
        Hand h = new Hand();
        for (int n = 0; n < 5; n++) {
            Card c = new Card(Card.Rank.getRandomRank(), Card.Suit.getRandomSuit());
            h.addCard(c);
        }
        h.calculateHandValue();
        //Question 2
        System.out.println("QUESTION 2");
        System.out.println(h);
        System.out.println("The number of clubs in the hand is: "
                + h.suitValues[0]);
        System.out.println("The number of diamonds in the hand is: "
                + h.suitValues[1]);
        System.out.println("The number of hearts in the hand is: "
                + h.suitValues[2]);
        System.out.println("The number of spades in the hand is: "
                + h.suitValues[3] + "\n");

        //Adding a test card
        Card testCard = new Card(Card.Rank.getRandomRank(), Card.Suit.DIAMONDS);
        System.out.println("Card to be added: " + testCard);
        h.addCard(testCard);

        //Demonstrating that countSuits works
        System.out.println("The number of diamonds in the hand is: "
                + h.countSuits(Card.Suit.DIAMONDS));

        //Question 3
        System.out.println("QUESTION 3");
        System.out.println("Print serialVersionUID: " + serialVersionUID);
        saveHand();
        
        //Question 4
        System.out.println("QUESTION 4");

        //Adding an ace to ensure that the hand has 2 values to demonstrate
        //calculateHandValue working
        Card ace = new Card(Card.Rank.ACE, Card.Suit.getRandomSuit());
        h.addCard(ace);
        System.out.println(h);
        h.calculateHandValue();
        System.out.println("Hand Values with AH and AL, in High to Low order: ");
        for (int i = 0; i < 5; i++) {
            if (h.handValues[i] != 0) {
                System.out.print(h.handValues[i] + " ");
            }
        }
        System.out.println("");
        //Question 5
        System.out.println("\nQUESTION 5");
        System.out.println(h);

        //Creating elements to add
        ArrayList<Card> testcardsAdd;
        testcardsAdd = new ArrayList<>();
        for (int n = 0; n < 3; n++) {
            testcardsAdd.add(new Card(Card.Rank.getRandomRank(), Card.Suit.getRandomSuit()));
        }
        Card addCardTest = new Card(Card.Rank.getRandomRank(), Card.Suit.getRandomSuit());
        Hand addHandTest = new Hand();

        //Filling the test Hand
        for (int n = 0; n < 4; n++) {
            Card c = new Card(Card.Rank.getRandomRank(), Card.Suit.getRandomSuit());
            addHandTest.addCard(c);
        }

        System.out.println("Cards that will be added");
        System.out.println("Single Card to be added: " + addCardTest);
        System.out.println("Collection of cards to be added: " + testcardsAdd);
        System.out.println("Hand to be added\n" + addHandTest);

        //Performing add card methods
        h.addCard(addCardTest);
        h.addCollection(testcardsAdd);
        h.addHand(addHandTest);
        System.out.println("Hand after cards have been added");
        System.out.println(h);

        //Question 6
        System.out.println("QUESTION 6");
        Hand h2 = new Hand();

        //Creating test hand
        for (int n = 0; n < 8; n++) {
            Card c = new Card(Card.Rank.getRandomRank(), Card.Suit.getRandomSuit());
            h2.addCard(c);
        }

        System.out.println(h2);
        Card removeCardTest = new Card(h2.hand.get(0).getRank(), h2.hand.get(0).getSuit());
        Hand removeHandTest = new Hand();

        for (int n = 5; n < 8; n++) {
            Card c = new Card(h2.hand.get(n).getRank(), h2.hand.get(n).getSuit());
            removeHandTest.addCard(c);
        }

        System.out.println("Cards that will be removed");
        System.out.println("Single Card to be removed: " + removeCardTest);
        System.out.println("Card at 1 index to be removed: " + h2.hand.get(1));
        System.out.println("Hand to be removed\n" + removeHandTest);

        //Performing card removals
        h2.removeCard(removeCardTest);
        h2.removeCardatIndex(0);
        h2.removeHand(removeHandTest);

        System.out.println("Hand after cards have been removed");
        System.out.println(h2);

        //Question 8
        System.out.println("QUESTION 8");
        Hand h3 = new Hand();
        for (int n = 0; n < 10; n++) {
            Card c = new Card(Card.Rank.getRandomRank(), Card.Suit.getRandomSuit());
            h3.addCard(c);
        }

        //sortByRank
        System.out.println("Hand before sorting: ");
        System.out.println(h3);
        System.out.println("Hand after sortByRank: ");
        h3.sortByRank();
        System.out.println(h3);

        //sort
        h3.sort();
        System.out.println("Hand after sorting: ");
        System.out.println(h3);

        //countSuit
        System.out.println("Testing countSuit Method");
        System.out.println("The number of CLUBS in the hand is: "
                + h3.countSuits(Card.Suit.CLUBS));

        //countRank
        System.out.println("Testing countRank Method");
        System.out.println("The number of " + h3.hand.get(0).getRank()
                + "'s in the hand is : " + h3.countRank(h3.hand.get(0).getRank()));

        //hasSuit
        System.out.println("Testing hasSuit Method");
        System.out.println("Does this hand contain SPADES: "
                + h3.hasSuit(Card.Suit.SPADES));

    }

}
