///////////////////////////////////////////////////////////////////////////////
// Main Class File:  Card.java
// File:             Card.java
// Date:             23/01/2019
//
// Author:           Christopher Fleetwood
// Student Number:   100228021
//
// Description: This class is designed to model a playing card and provide
//              useful methods to allow manipulation of the card data.
//////////////////////////// 80 columns wide //////////////////////////////////
package cards;

//Importing the required java files
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Random;

//Implementing the appropriate interfaces
public class Card implements Serializable, Comparable<Card> {

    //Defining an enum class to include relevant variables and methods
    //related to the rank of the cards
    public enum Rank {
        TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7), EIGHT(8), NINE(9),
        TEN(10), JACK(10), QUEEN(10), KING(10), ACE(11);
        private final int rankVal;

        //Constructor
        private Rank(int rankVal) {
            this.rankVal = rankVal;
        }

        //Returns the next card in the enum eg calling the method
        //on JACK returns QUEEN
        public Rank getNext() {
            //TERNARY OPERATOR
            return this.ordinal() < Rank.values().length - 1
                    ? Rank.values()[this.ordinal() + 1]
                    : Rank.ACE;
        }

        //returns a random Rank, useful for testing
        public static Rank getRandomRank() {
            Random random = new Random();
            return values()[random.nextInt(values().length)];
        }

    }

    //Defining an enum class to include relevant variables and methods
    //related to the rank of the cards
    public enum Suit {
        CLUBS, DIAMONDS, HEARTS, SPADES;

        //Returns a random suit, useful for testing
        public static Suit getRandomSuit() {
            Random random = new Random();
            return values()[random.nextInt(values().length)];
        }

    }

    //Initializing variables
    private static final long serialVersionUID = 100;
    private final Rank rank;
    private final Suit suit;

    //Constructor
    public Card(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    //ACCESSORS
    public Rank getRank() {
        return this.rank;

    }

    public Suit getSuit() {
        return this.suit;
    }

    public int getValue() {
        return this.rank.rankVal;
    }

    //Prints out the rank and suit of each card
    @Override
    public String toString() {
        String cString;
        cString = (getRank()
                + " OF " + this.suit);
        return cString;
    }

    //Returns the highest value card in a list of cards
    public static Card max(ArrayList<Card> cardList) {
        //Using the default iterator to iterate through the ArrayList
        Iterator<Card> it = cardList.iterator();

        //Initializing needed cards
        Card maxCard = null;
        Card tempCard = null;

        while (it.hasNext()) {
            if (maxCard == null) {
                maxCard = it.next();
            } else {
                tempCard = it.next();
                if (tempCard.compareTo(maxCard) > 0) {
                    maxCard = tempCard;
                }
            }
        }
        return maxCard;
    }

    //Overriding the default compare to method to allow Card objects to be 
    //compared
    @Override
    public int compareTo(Card c) {
        if (this.rank.ordinal() > c.rank.ordinal()) {
            return 1;
        } else if (this.rank.ordinal() < c.rank.ordinal()) {
            return -1;
        } else if (this.rank.ordinal() == c.rank.ordinal()) {
            if (this.suit.ordinal() > c.suit.ordinal()) {
                return 2;
            } else if (this.suit.ordinal() < c.suit.ordinal()) {
                return -2;
            }
        }
        return 0;
    }

//CompareDescending is used to sort the cards into descending order, where
//the rank descreases but the suit is the same as usual
//EG Before: KingH, QueenC, KingD, QueenS
//   After: KingD, KingH, QueenC, QueenS
    public static class CompareDescending implements Comparator<Card> {

        @Override
        public int compare(Card C1, Card C2) {
            switch (C1.compareTo(C2)) {
                case 1:
                    return -1;
                case -1:
                    return 1;
                default:
                    return C1.compareTo(C2);
            }
        }
    }

    //CompareRank sorts ascending by rank but maintains suit in insertion
    //order
    public static class CompareRank implements Comparator<Card> {

        @Override
        public int compare(Card C1, Card C2) {
            if (C1.compareTo(C2) == 1) {
                return 1;
            } else if (C1.compareTo(C2) == -1) {
                return -1;
            }
            return 0;
        }
    }

//This method returns an arraylist of cards that are greater than the card
//arguement that is passed, as decided by the comparator that is passed
    public static ArrayList<Card> chooseGreater(List<Card> cardList, Comparator<Card> C, Card C1) {
        ArrayList<Card> greaterThan = new ArrayList<>();
        Iterator<Card> it = cardList.iterator();

        while (it.hasNext()) {
            Card c;
            if (C.compare(C1, (c = it.next())) == 1) {
                greaterThan.add(c);
            }
        }
        return greaterThan;
    }

    //Demonstrating my understanding of lambdas
    public static void selectTest() {
        
        Comparator<Card> CD = new CompareDescending();
        Comparator<Card> CR = new CompareRank();

        //Initializing a test List
        ArrayList<Card> testList = new ArrayList<>();
        testList.add(new Card(Rank.TWO, Suit.SPADES));
        testList.add(new Card(Rank.FOUR, Suit.CLUBS));
        testList.add(new Card(Rank.SEVEN, Suit.HEARTS));
        testList.add(new Card(Rank.EIGHT, Suit.DIAMONDS));
        testList.add(new Card(Rank.THREE, Suit.DIAMONDS));
        testList.add(new Card(Rank.QUEEN, Suit.SPADES));
        testList.add(new Card(Rank.KING, Suit.CLUBS));
        testList.add(new Card(Rank.TWO, Suit.HEARTS));
        
        Card inspectedCard = new Card(Rank.THREE, Suit.HEARTS);
        
        ArrayList<Card> al = new ArrayList<>();
        
        //Lambda Expression
        testList.forEach(item -> {
            if (CR.compare(inspectedCard, item) == -1){
                al.add(item);
            }
            if (CR.compare(inspectedCard, item) == 0){
                if (CD.compare(inspectedCard, item) == -1){
                    al.add(item);
                }
            }
        });
        
        //Outputting the arrayList of added Cards
        System.out.println(al.toString());
    }

    //Overloading the equals method so that cards of the same rank and suit
    //but not identical (memory address) are defined as equal by the
    //program. This method is required otherwise the hand.remove method
    //does not function correctly
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!Card.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        Card card = (Card) obj;

        return this.compareTo(card) == 0;
    }

    //hashCode method is required in order to overload the equals method
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + Objects.hashCode(this.rank);
        hash = 31 * hash + Objects.hashCode(this.suit);
        return hash;
    }

    public static void saveCard() {
        String filename = "card.ser";
        Card saveCard = new Card(Rank.ACE, Suit.DIAMONDS);
        try {
            FileOutputStream fos = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(fos);
            out.writeObject(saveCard);
            out.close();
            fos.close();
            System.out.println("Serialized Data saved in: " + filename);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //TEST HARNESS
    public static void main(String[] args) {
        //Question 1
        System.out.println("QUESTION 1");
        System.out.println("Serialization ID of 100");
        saveCard();

        //Question 2 AND 3
        System.out.println("\nQUESTION 2 AND 3");
        Card testCard = new Card(Rank.SEVEN, Suit.CLUBS);
        System.out.println("Test Card toString: " + testCard);
        System.out.println("GetNext: " + testCard.rank.getNext());
        System.out.println("Get Test Card Value: " + testCard.getValue());
        System.out.println("Test GetRandomSuit: " + new Card(Rank.ACE, Suit.getRandomSuit()) + "\n");

        //Question 4
        System.out.println("QUESTION 4");
        Card testCard2 = new Card(Rank.getRandomRank(), Suit.getRandomSuit());
        System.out.println("Test Card to be compared to: " + testCard);
        System.out.println("Card for Comparison: " + testCard2);
        System.out.println("Output of compareTo: " + testCard.compareTo(testCard2) + "\n");

        //Question 5
        System.out.println("QUESTION 5");
        //Generating a test list to find the max card in
        ArrayList<Card> testList = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            Card C3 = new Card(Rank.getRandomRank(), Suit.getRandomSuit());
            testList.add(C3);
        }
        testList.add(new Card(Rank.KING, Suit.SPADES));
        testList.add(new Card(Rank.KING, Suit.HEARTS));
        System.out.println("Test List of Cards:" + testList);
        System.out.println("Highest Card in Test List: " + max(testList) + "\n");

        //Question 6
        System.out.println("QUESTION 6");
        Comparator<Card> byRank = new CompareRank();
        Comparator<Card> byDescending = new CompareDescending();
        //Printing my output
        System.out.println("Test List to be sorted:                  "
                + testList);
        Collections.sort(testList, byRank);
        System.out.println("Test List sorted with CompareRank:       "
                + testList);
        Collections.sort(testList, byDescending);
        System.out.println("Test List sorted with CompareDescending: "
                + testList);

        //Question 7
        System.out.println("\nQUESTION 7");
        System.out.println("Printing out a Test List: " + testList);
        System.out.println("Printing out the Test Card: " + testCard);
        System.out.println("Printing out a list of cards greater than the Test "
                + "Card using CompareDescending: " + chooseGreater(testList, byDescending, testCard));

        //Question 8
        System.out.println("\nQUESTION 8");
        System.out.println("Performing the selectTest method on 3H: ");
        selectTest();
    }

}
