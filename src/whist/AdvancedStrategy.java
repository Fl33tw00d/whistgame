///////////////////////////////////////////////////////////////////////////////                 
// Main Class File:  BasicGame.java
// File:             AdvancedStrategy.java
// Date:             03/02/2019
//
// Author:           Christopher Fleetwood
// Student Number:   100228021
//
// Description: This class is designed to make decisions on which card should 
//              be played depending on a series of set rules
//////////////////////////// 80 columns wide //////////////////////////////////
package whist;

import cards.Card;
import cards.Hand;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Random;

/**
 *
 * @author vhe17qgu
 */
public class AdvancedStrategy implements Strategy {

    public AdvancedStrategy() {

    }

    //A method that runs through a sequence of logic to allow the computer to 
    //select cards to play whist 
    @Override
    public Card chooseCard(Hand h, Trick t) {
        //Method Specific Variables
        ArrayList<Card> highestValCards = new ArrayList<>();
        ArrayList<Card> sameSuitCards = new ArrayList<>();
        ArrayList<Card> nonTrumps = new ArrayList<>();
        Card[] trickCards = t.getCardsPlayed();
        Comparator<Card> byRank = new Card.CompareRank();
        int handSize = h.getHand().size();
        Card chosenCard = null;
        int playerOrdinal = 0;
        int partnerID;

        //Determining what position the player is in
        for (int j = 0; j < 4; j++) {
            if (trickCards[j] != null) {
                playerOrdinal++;
            }
        }
        //Sorting by rank to allow for easier card selection
        h.sortByRank();

        //If you're the lead player, then play the highest possible card
        if (playerOrdinal == 0) {
            //Adds all of the highest rank cards to an array list
            highestValCards = h.getHighCards(h.getHand().get(handSize - 1).getRank());
            //Then randomly selects a card of the highest possible rank
            chosenCard = highestValCards.get(new Random().nextInt(highestValCards.size()));
            h.removeCard(chosenCard);
            return chosenCard;
        }

        partnerID = (((t.getLeadPlayer() + 2) + playerOrdinal) % 4);
        //If the number of cards played is > 2, then you must be player 3 or 4
        //Then checks to see if the partner has the best card or has player
        if (t.getNumberOfCardsPlayed() >= 2
                && trickCards[partnerID] == t.getBestCard()) {
            //If they have the lead suit and their partner is winning
            //then they play the lowest possible card of lead suit
            if (h.hasSuit(t.getLeadSuit())) {
                sameSuitCards = h.getSuitedCards(t.getLeadSuit());
                chosenCard = sameSuitCards.get(0);
                //If they do not have lead suit, then they check to see if
                //the hand contains any non trump cards
            } else if (!h.getNonTrumps(t.getTrumps()).isEmpty()) {
                nonTrumps = h.getNonTrumps(t.getTrumps());
                chosenCard = nonTrumps.get(0);
            } else {
                //If the only option is to play a trump even though their
                //partner is winning, then it plays the highest possible trump
                chosenCard = h.getHand().get(handSize - 1);
            }
            //If the number of cards played is less than 2 or if they're not
            //winning, then tries to follow suit and beat the card
        } else if (h.hasSuit(t.getLeadSuit())) {
            //If it can beat the Suit, then it plays it the highest still
            //winning card
            sameSuitCards = h.getSuitedCards(t.getLeadSuit());
            Iterator<Card> it = sameSuitCards.iterator();

            boolean lowestWinner = false;
            while (it.hasNext() && !lowestWinner) {
                Card inspectedCard = it.next();
                if (byRank.compare(t.getBestCard(), inspectedCard) == -1) {
                    chosenCard = inspectedCard;
                    lowestWinner = true;
                } else {
                    lowestWinner = false;
                }
            }

            if (chosenCard == null) {
                chosenCard = sameSuitCards.get(0);
            }

            //If we cannot follow suit, then we check if it has a trump suit
        } else if (h.hasSuit(t.getTrumps())) {
            sameSuitCards = h.getSuitedCards(t.getTrumps());
            Iterator<Card> it = sameSuitCards.iterator();

            //If the Advanced player cannot follow suit, it plays the lowest
            //possible trump that still allows it to win.
            //This allows for more high cards to be saved for later games
            //Since we know this is player 3 or 4, then it is likely that if 
            //you're player 3, then the player 4 may still be able to follow 
            //suit so there is no point playing a high trump.
            //If you're player 4 then it means that it is playing the lowest
            //trump and is guaranteed to win anyway since everyone else
            //has followed suit.
            //This is the factor that most strongly affect win rate.
            boolean lowestWinner = false;
            while (it.hasNext() && !lowestWinner) {
                Card inspectedCard = it.next();
                if (byRank.compare(t.getBestCard(), inspectedCard) == -1) {
                    chosenCard = inspectedCard;
                    lowestWinner = true;
                } else {
                    lowestWinner = false;
                }
            }

            //If it cannot beat the best card with
            if (chosenCard == null) {
                chosenCard = sameSuitCards.get(0);
            }

        } else {
            //If partner is not winning, hand does not have lead suit or a trump
            //then it discards the lowest non trump possible
            chosenCard = h.getHand().get(0);
        }

        //Removes the card from the hand and returns it
        h.removeCard(chosenCard);
        return chosenCard;
    }

    //Update Data
    @Override
    public void updateData(Trick c) {
        //NOT REQUIRED
    }

    public static void main(String[] args) {
    }

}
