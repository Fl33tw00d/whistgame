///////////////////////////////////////////////////////////////////////////////                 
// Main Class File:  BasicGame.java
// File:             HumanStrategy.java
// Date:             02/02/2019
//
// Author:           Christopher Fleetwood
// Student Number:   100228021
//
// Description: This class is designed to allow a human to play against the 
//              computer, it is mostly there to take user input.
//////////////////////////// 80 columns wide //////////////////////////////////
package whist;

import cards.Card;
import cards.Hand;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author vhe17qgu
 */
public class HumanStrategy implements Strategy {

    @Override
    public Card chooseCard(Hand h, Trick t) {
        Card chosenCard = null;
        //Chose to use scanner over BufferedReader as scanner tokenizes the 
        //input and allows us to just check for nextInt doing lots of the
        //work for us
        Scanner scan = new Scanner(System.in);
        int userInput = 0;
        int humanOrdinal = 0;
        ArrayList<Card> allowedCards = new ArrayList<>();
        h.sort();

        //OUTPUTTING TO THE PLAYER
        System.out.println("Trick: ");
        System.out.println(t);
        System.out.println("Trumps: " + t.getTrumps());
        System.out.println("Your Cards: ");
        //This is the loop that prints out the numbers and cards
        //that the player can select
        for (int i = 0; i < h.getHand().size(); i++) {
            System.out.print(String.format("%-2d: %s", i, h.getHand().get(i) + "\n"));
        }

        //Determines which player the human is
        for (int j = 0; j < 4; j++) {
            if (t.getCardsPlayed()[j] != null) {
                humanOrdinal++;
            }
        }
        System.out.println("You are player: " + (humanOrdinal + 1));

        //If human is not the first player then must follow suit
        //So we create a list of allowed cards
        if (humanOrdinal != 0 && h.hasSuit(t.getLeadSuit())) {
            allowedCards.addAll(h.getSuitedCards(t.getLeadSuit()));
        } else {
            allowedCards.addAll(h.getHand());
        }

        //This is the loop where the player inputs
        while (chosenCard == null) {
            System.out.println("Please input the no. of card you would like to"
                    + " select: ");
            while (!scan.hasNextInt()) {
                //Sanitizing input
                System.out.println("Please input a valid integer");
                scan.next();
            }

            userInput = scan.nextInt();

            if (allowedCards.contains(h.getHand().get(userInput))) {
                chosenCard = h.getHand().get(userInput);
            } else {
                System.out.println("Please select a valid card to play");
            }
        }
        h.removeCard(chosenCard);
        return chosenCard;
    }

    @Override
    public void updateData(Trick c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
