///////////////////////////////////////////////////////////////////////////////                 
// Main Class File:  BasicWhist.java
// File:             BasicWhist.java
// Date:             01/02/2019
//
// Author:           Christopher Fleetwood
// Student Number:   100228021
//
// Description: This class is designed to simulate a game of Whist.
//////////////////////////// 80 columns wide //////////////////////////////////
package whist;

import cards.Deck;
import cards.Card.Suit;

/**
 *
 * @author ajb
 */
public class BasicWhist {

    static final int NOS_PLAYERS = 4;
    static final int NOS_TRICKS = 13;
    static final int WINNING_POINTS = 7;
    int team1Points = 0;
    int team2Points = 0;
    Player[] players;
    //Stores temporary points
    int trickPoints[] = new int[2];

    //Constructor for the basic game
    public BasicWhist(Player[] pl) {
        this.players = pl;
    }

    //Deals the hands to each of the plays
    public void dealHands(Deck newDeck) {
        //NOS_TRICKS SHOULD BE ACTUALLY 52
        for (int i = 0; i < NOS_TRICKS * 4; i++) {
            players[i % NOS_PLAYERS].dealCard(newDeck.deal());
        }
    }

    //Plays an individual trick (round of 4 cards)
    public Trick playTrick(Player firstPlayer) {
        Trick t = new Trick(firstPlayer.getID());
        int playerID = firstPlayer.getID();
        for (int i = 0; i < NOS_PLAYERS; i++) {
            int next = (playerID + i) % NOS_PLAYERS;
            t.setCard(players[next].playCard(t), players[next]);
        }
        return t;
    }

    public void playGame() {
        Deck d = new Deck();
        dealHands(d);
        int firstPlayer = (int) (NOS_PLAYERS * Math.random());

        Suit trumps = Suit.getRandomSuit();
        Trick.setTrumps(trumps);

        for (int j = 0; j < NOS_PLAYERS; j++) {
            players[j].setTrumps(trumps);
        }

        for (int i = 0; i < NOS_TRICKS; i++) {
            Trick t = playTrick(players[firstPlayer]);
            System.out.println("Trump: " + t.getTrumps());
            System.out.println("    Trick: " + "\n" + t);
            firstPlayer = t.findWinner();
            addPoints(firstPlayer);
            System.out.println("Winning PlayerID: " + firstPlayer + "\n");
        }
        //If they have over 6 points then add it to final score
        if (trickPoints[0] > 6) {
            System.out.println("Team 1 gains: " + (trickPoints[0] - 6));
            team1Points = team1Points + (trickPoints[0] - 6);
        }

        if (trickPoints[1] > 6) {
            System.out.println("Team 2 gains: " + (trickPoints[1] - 6));
            team2Points = team2Points + (trickPoints[1] - 6);
        }
        trickPoints[0] = 0;
        trickPoints[1] = 0;
    }

    public void playMatch() {
        team1Points = 0;
        team2Points = 0;

        while (team1Points < WINNING_POINTS && team2Points < WINNING_POINTS) {
            playGame();
        }
        if (team1Points >= WINNING_POINTS) {
            System.out.println("Winning team is team 1 with: " + team1Points);
            System.out.println("Losing team is team 2 with: " + team2Points);
        } else {
            System.out.println("Winning team is team 2 with: " + team2Points);
            System.out.println("Losing team is team 1 with: " + team1Points);
        }
    }

    public static void playBasicGame() {
        Player[] p = new Player[NOS_PLAYERS];
        for (int i = 0; i < p.length; i++) {
            p[i] = new BasicPlayer(i);//CREATE YOUR PLAYERS HERE
            p[i].setStrategy(new BasicStrategy());
        }
        BasicWhist bg = new BasicWhist(p);
        bg.playMatch(); //Just plays a single match
    }

    public static void playHumanGame() {
        Player[] p = new Player[NOS_PLAYERS];
        for (int i = 0; i < p.length; i++) {
            p[i] = new BasicPlayer(i);//CREATE YOUR PLAYERS HERE
        }

        for (int j = 0; j < p.length; j++) {
            p[j].setStrategy(new BasicStrategy());
        }

        p[0].setStrategy(new HumanStrategy());

        BasicWhist bg = new BasicWhist(p);
        System.out.println("\nWELCOME TO WHIST");
        System.out.println("Your PlayerID is: 0");
        System.out.println("Your Partner's PlayerID is: 2");
        bg.playMatch(); //Just plays a single match
    }

    public static int playAdvancedGame() {
        Player[] p = new Player[NOS_PLAYERS];
        for (int i = 0; i < p.length; i++) {
            p[i] = new BasicPlayer(i);//CREATE YOUR PLAYERS HERE
        }

        //Assigning 2 players to basic 
        //and 2 players to advanced
        for (int j = 0; j < p.length; j++) {
            //Even are Basic
            if (j % 2 == 0) {
                p[j].setStrategy(new BasicStrategy());
            } else {
                //Odd are Advanced
                p[j].setStrategy(new AdvancedStrategy());
            }
        }
        BasicWhist bg = new BasicWhist(p);
        bg.playMatch(); //Just plays a single match

        //If basic wins, return a 0, if advanced wins return a 1
        if (bg.team1Points >= WINNING_POINTS) {
            return 0;
        } else {
            return 1;
        }

    }

    public static void main(String[] args) {
        //playBasicGame();
        //playHumanGame();
        //playAdvancedGame();
        
        //Stores the number of wins
        int advancedWins = 0;
        int basicWins = 0;

        //Running the desired number of games
        for (int i = 0; i < 100; i++) {
            //Summing the scores
            if (playAdvancedGame() == 0) {
                basicWins++;
            } else {
                advancedWins++;
            }
        }
        
        System.out.println("Basic wins: " + basicWins
                + "\nAdvanced wins: "+ advancedWins);
    }

    //Adds the points to the required team
    public void addPoints(int winner) {
        if (winner == 0 || winner == 2) {
            trickPoints[0]++;
        }
        if (winner == 1 || winner == 3) {
            trickPoints[1]++;
        }

    }

}
