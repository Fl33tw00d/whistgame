///////////////////////////////////////////////////////////////////////////////                 
// Main Class File:  BasicGame.java
// File:             BasicPlayer.java
// Date:             02/02/2019
//
// Author:           Christopher Fleetwood
// Student Number:   100228021
//
// Description: This class is designed to player in a game of WHIST. It stores 
//              all of the relevant information and provides useful
//              methods for maniuplating the data.
//////////////////////////// 80 columns wide //////////////////////////////////
package whist;

import cards.Card;
import cards.Card.Suit;
import cards.Hand;

/**
 *
 * @author vhe17qgu
 */
public class BasicPlayer implements Player {

    private Hand hand;
    private Strategy strategy;
    private int ID;
    private int partnerID;
    private Suit trumps;

    public BasicPlayer(int playerID) {
        this.hand = new Hand();
        this.ID = playerID;
        this.partnerID = (playerID + 2) % 4;
    }

    //Adds card c to this players hand
    @Override
    public void dealCard(Card c) {
        hand.addCard(c);
    }

    //Allows for external setting of player strategy, either Basic,
    //Human or Advanced
    @Override
    public void setStrategy(Strategy s) {
        this.strategy = s;
    }

    //Determines which of the players cards to play based on the in play trick t 
    //and player strategy
    @Override
    public Card playCard(Trick t) {
        return strategy.chooseCard(hand, t);
    }

    //Game passes the players the completed trick 
    @Override
    public void viewTrick(Trick t) {
        this.strategy.updateData(t);
    }

    //Sets the trumps in the players
    @Override
    public void setTrumps(Card.Suit s) {
        trumps = s;
    }

    //returns the playerID
    @Override
    public int getID() {
        return ID;
    }

}
