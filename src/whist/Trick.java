///////////////////////////////////////////////////////////////////////////////
// Main Class File:  BasicGame.java
// File:             Trick.java
// Date:             28/01/2019
//
// Author:           Christopher Fleetwood
// Student Number:   100228021
//
// Description: This class is designed to model a trick and provide
//              useful methods to allow manipulation of the trick data.
//////////////////////////// 80 columns wide //////////////////////////////////
package whist;

import cards.Card;
import cards.Card.Suit;
import cards.Card.Rank;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

/**
 * Skeleton class for storing information about whist tricks
 *
 * @author ajb
 */
public class Trick {

    //INITALIZING VARIABLES
    public static Suit trumps;
    private Card[] cardsPlayed;
    private int leadPlayer;
    private int trickWinner;

    //CONSTRUCTOR
    public Trick(int p) {
        this.leadPlayer = p;
        this.cardsPlayed = new Card[4];
    }    //p is the lead player 

    //Sets the trump suit for the trick
    public static void setTrumps(Suit s) {
        trumps = s;
    }

    //Returns the trump suit for the trick
    public Suit getTrumps() {
        return trumps;
    }

    //Returns the suit of the first card played
    public Suit getLeadSuit() {
        return cardsPlayed[leadPlayer].getSuit();
    }

    //Sets the card played by the player in the trick
    public void setCard(Card c, Player p) {
        cardsPlayed[p.getID()] = c;
    }

    //Returns the card played the player in the trick
    public Card getCard(Player p) {
        return cardsPlayed[p.getID()];
    }

    public int getPartnerID(Player p) {
        return (p.getID() + 2) % 4;
    }

    //Returns the first player in the trick
    public int getLeadPlayer() {
        return leadPlayer;
    }

    //Returns an array of cards played in the trick
    public Card[] getCardsPlayed() {
        return cardsPlayed;
    }

    //Returns the number of cards that have so far been played in the trick
    public int getNumberOfCardsPlayed() {
        int numCardsPlayed = 0;
        for (Card c : cardsPlayed) {
            if (c != null) {
                numCardsPlayed++;
            }
        }
        return numCardsPlayed;
    }

    //Returns the winner of the trick
    public int getTrickWinner() {
        return trickWinner;
    }

    //This method formats the trick and outputs
    @Override
    public String toString() {
        int i = 0;
        StringBuilder trickString = new StringBuilder("");
        for (Card c : cardsPlayed) {
            if (cardsPlayed[i] != null) {
                trickString.append(c.toString()).append(System.lineSeparator());
                i++;
            } else {
                //If there is not a card there, then must print empty
                //so that the user can see where they are in the order
                System.out.println("EMPTY");
                i++;
            }
        }
        return trickString.toString();
    }

    //Returns the current best card to have been played in the trick so far
    public Card getBestCard() {
        int bestCard = leadPlayer;
        Comparator<Card> byRank = new Card.CompareRank();

        for (int i = 0; i < 4; i++) {
            int currentPlayer = ((leadPlayer + i) % 4);
            if (cardsPlayed[currentPlayer] == null) {
                break;
            }
            //If they are the same suit as the current winning card
            if (cardsPlayed[currentPlayer].getSuit().compareTo(cardsPlayed[bestCard].getSuit()) == 0) {
                //Compare the rank, if it's a higher rank then they become the best card
                if (byRank.compare(cardsPlayed[currentPlayer], cardsPlayed[bestCard]) == 1) {
                    bestCard = currentPlayer;
                }
                //If the inspected card is trumps, and the current winning card
                //is not a trump, then they are the winner
            } else if ((cardsPlayed[currentPlayer].getSuit().compareTo(trumps) == 0)
                    && (cardsPlayed[currentPlayer].getSuit() != cardsPlayed[bestCard].getSuit())) {
                bestCard = currentPlayer;
            }
        }
        return cardsPlayed[bestCard];
    }

    /**
     * Finds the ID of the winner of a completed trick
     *
     * @return winner
     */
    //Sort through the cards finding the highest card
    public int findWinner() {
        int winner = leadPlayer;
        Comparator<Card> byRank = new Card.CompareRank();

        //If this works change trick[i].getSuit() to variable currentSuit to 
        //clean up code make it more readable
        for (int i = 0; i < 4; i++) {
            if (cardsPlayed[i].getSuit().compareTo(cardsPlayed[winner].getSuit()) == 0) {
                //Compare the rank, if it's a higher rank then they become the winning card
                if (byRank.compare(cardsPlayed[i], cardsPlayed[winner]) == 1) {
                    winner = i;
                }
                //If the inspected card is trumps, and the current winning card
                //is not a trump, then they are the winner
            } else if ((cardsPlayed[i].getSuit().compareTo(trumps) == 0)
                    && (cardsPlayed[i].getSuit() != cardsPlayed[winner].getSuit())) {
                winner = i;
            }
        }

        trickWinner = winner;
        return winner;
    }
}
