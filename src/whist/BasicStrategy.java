///////////////////////////////////////////////////////////////////////////////                 
// Main Class File:  BasicGame.java
// File:             BasicStrategy.java
// Date:             01/01/2019
//
// Author:           Christopher Fleetwood
// Student Number:   100228021
//
// Description: This class is designed to make decisions on which card should 
//              be played depending on a series of set rules
//////////////////////////// 80 columns wide //////////////////////////////////
package whist;

import cards.Card;
import cards.Hand;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

/**
 *
 * @author vhe17qgu
 */
public class BasicStrategy implements Strategy {

    public BasicStrategy() {

    }

    //A method that runs through a sequence of logic to allow the computer to 
    //select cards to play whist 
    @Override
    public Card chooseCard(Hand h, Trick t) {
        //VARIABLES NEEDED
        ArrayList<Card> highestValCards = new ArrayList<>();
        ArrayList<Card> sameSuitCards = new ArrayList<>();
        ArrayList<Card> nonTrumps = new ArrayList<>();
        Card[] trickCards = t.getCardsPlayed();
        Comparator<Card> byRank = new Card.CompareRank();
        int handSize = h.getHand().size();
        Card chosenCard = null;
        int playerOrdinal = 0;
        int partnerID;

        //Determining what position the player is in
        for (int j = 0; j < 4; j++) {
            if (trickCards[j] != null) {
                playerOrdinal++;
            }
        }
        //Sorting by rank to allow for easier card selection
        h.sortByRank();

        //If you're the lead player, then play the highest possible card
        if (playerOrdinal == 0) {
            highestValCards = h.getHighCards(h.getHand().get(handSize - 1).getRank());
            //This size may not be minus
            chosenCard = highestValCards.get(new Random().nextInt(highestValCards.size()));
            h.removeCard(chosenCard);
            return chosenCard;
        }
        
        //This calculates your partner ID based on your ordinal and the
        //lead player
        partnerID = (((t.getLeadPlayer() + 2) + playerOrdinal) % 4);
        //If the number of cards played is > 2, then you must be player 3 or 4
        //Then checks to see if the partner has the best card or has player
        if (t.getNumberOfCardsPlayed() >= 2
                && trickCards[partnerID] == t.getBestCard()) {
            //If they have the lead suit and their partner is winning
            //then they play the lowest possible card of lead suit
            if (h.hasSuit(t.getLeadSuit())) {
                sameSuitCards = h.getSuitedCards(t.getLeadSuit());
                chosenCard = sameSuitCards.get(0);
                //If they do not have lead suit, then they check to see if
                //the hand contains any non trump cards
            } else if (!h.getNonTrumps(t.getTrumps()).isEmpty()) {
                nonTrumps = h.getNonTrumps(t.getTrumps());
                chosenCard = nonTrumps.get(0);
            } else {
                //If the only option is to play a trump even though their
                //partner is winning, then it plays the highest possible trump
                chosenCard = h.getHand().get(handSize - 1);
            }
            //If the number of cards played is less than 2 or if they're not
            //winning, then tries to follow suit and beat the card
        } else if (h.hasSuit(t.getLeadSuit())) {
            sameSuitCards = h.getSuitedCards(t.getLeadSuit());
            //If it can beat the Suit, then it plays it the highest card
            //it can to win
            if (byRank.compare(t.getBestCard(),
                    sameSuitCards.get(sameSuitCards.size() - 1)) == -1) {
                chosenCard = sameSuitCards.get(sameSuitCards.size() - 1);
            } else {
                //If it does not have a card better than the best card then it
                //plays the lowest card of the same Suit
                chosenCard = sameSuitCards.get(0);
            }
            //If we cannot follow suit, then we check if it has a trump suit
            //If it does, then it plays the highest possible trump card
        } else if (h.hasSuit(t.getTrumps())) {
            sameSuitCards = h.getSuitedCards(t.getTrumps());
            chosenCard = sameSuitCards.get(sameSuitCards.size() - 1);

        } else {
            //If partner is not winning, hand does not have lead suit or a trump
            //then it discards the lowest non trump possible
            chosenCard = h.getHand().get(0);
        }

        //Removes the card from the hand and returns it
        h.removeCard(chosenCard);
        return chosenCard;
    }

    @Override
    public void updateData(Trick c) {
        //NOT REQUIRED FOR BASIC STRATEGY
    }

    public static void main(String[] args) {
    }

}
